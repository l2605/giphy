package com.example.giphy.dto.response

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName = "images"
)
data class Images(
    @PrimaryKey(autoGenerate = true)
    var obId : Int?= null,
    val fixed_height: FixedHeight,
    val original: Original,

    // Took only two objects from the Image Object from the API
)