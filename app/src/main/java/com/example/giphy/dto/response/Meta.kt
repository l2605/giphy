package com.example.giphy.dto.response

data class Meta(
    val msg: String,
    val response_id: String,
    val status: Int
)