package com.example.giphy.dto.response

data class Pagination(
    val count: Int,
    val offset: Int,
    val total_count: Int
)