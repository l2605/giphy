package com.example.giphy.dto.response

data class GiphyResponse(
    val data: List<Giphy>,
    val meta: Meta,
    val pagination: Pagination
)