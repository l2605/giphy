package com.example.giphy.dto.response

data class FixedHeight(
    val height: String?=null,
    val mp4: String?=null,
    val mp4_size: String?=null,
    val size: String?=null,
    val url: String,
    val webp: String?=null,
    val webp_size: String?=null,
    val width: String?=null
)