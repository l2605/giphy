package com.example.giphy.application

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.example.giphy.room.GiphyDatabase
import timber.log.Timber
import java.io.File

class MainApplication : Application() {

    var TAG = "test"
    companion object {
        var instance: MainApplication? = null

    }
    override fun onCreate() {
        super.onCreate()
        instance = this

        initTimberLogger()
//        initRoom()
//        initObjectBox()
    }

    private fun initRoom() {

        GiphyDatabase.invoke(this)
    }

//    private fun initObjectBox() {
//        ObjectBoxInstance.init(this)
//
////        if (BuildConfig.DEBUG) {
//        Admin(ObjectBoxInstance.boxStore).start(this)
////        }
//    }

    private fun initTimberLogger() {
        //   if (BuildConfig.DEBUG) {
        // DebugTree has all usual logging functionality
        Timber.plant(object : Timber.DebugTree() {
            override fun createStackElementTag(element: StackTraceElement): String? {
                return String.format(
                    "[L:%s] [M:%s] [N:%s] [C:%s]",
                    element.lineNumber,
                    element.methodName,
                    element.className,
                    super.createStackElementTag(element)
                )
            }

            override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
                //change this tag for logging with filter in android studio...
                super.log(priority, TAG, message, t)
            }
        })
        //  }
    }


}