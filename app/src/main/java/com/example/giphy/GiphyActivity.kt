package com.example.giphy

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.giphy.adapter.ViewPagerAdapter
import com.example.giphy.application.MainApplication
import com.example.giphy.databinding.ActivityGifBinding
import com.example.giphy.repositories.GiphyRepository
import com.example.giphy.room.GiphyDatabase
import com.example.giphy.ui.vm.TrendingGifViewModel
import com.example.giphy.ui.vm.TrendingGifViewModelFactory
import com.google.android.material.tabs.TabLayoutMediator

class GiphyActivity : AppCompatActivity() {

    private lateinit var binding: ActivityGifBinding
    lateinit var viewmodel: TrendingGifViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityGifBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val giphyRepository = GiphyRepository.getInstance(this.application)
        val viewModelProviderFactory = TrendingGifViewModelFactory(giphyRepository)
        viewmodel = ViewModelProvider(this,viewModelProviderFactory).get(TrendingGifViewModel::class.java)
        binding.viewPager.adapter = ViewPagerAdapter(this)


        // Connect ViewPager with TabLayout
        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
            when (position) {
                0 -> tab.text = "Trending"
                1 -> tab.text = "Favourite"
            }
        }.attach()

    }
}