package com.example.giphy.ui.vm

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.giphy.repositories.GiphyRepository

class TrendingGifViewModelFactory(var repository: GiphyRepository) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TrendingGifViewModel::class.java)) {
            return TrendingGifViewModel( repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}