package com.example.giphy.ui.screen

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import com.example.giphy.GiphyActivity
import com.example.giphy.R
import com.example.giphy.adapter.TrendingGiphyAdapter
import com.example.giphy.databinding.FragmentFavouriteBinding
import com.example.giphy.dto.response.Giphy
import com.example.giphy.ui.vm.TrendingGifViewModel
import com.example.giphy.util.Constant
import com.example.giphy.util.GsonUtil
import com.example.giphy.util.Logger
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class FavouriteFragment : Fragment() {

    private var _binding: FragmentFavouriteBinding? = null
    lateinit var viewModel: TrendingGifViewModel
    lateinit var giphyAdapter: TrendingGiphyAdapter
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentFavouriteBinding.inflate(inflater, container, false)
        viewModel = (activity as GiphyActivity).viewmodel

        setupRecyclerView()


        viewModel.getFavouriteGiphy().observe(viewLifecycleOwner, Observer { giphies->
            giphyAdapter.differ.submitList(giphies)
        })
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = (activity as GiphyActivity).viewmodel
    }



    private fun removeFromFavouriteGiphyLocally(it: Giphy) {

        MainScope().launch(Dispatchers.Main) {
            delay(Constant.SEARCH_GIF_TIME_DELAY)
            viewModel.removeFromFavouriteGiphy(it)
            Toast.makeText(view?.context,"Removed from favourite",Toast.LENGTH_SHORT).show()
        }

    }


    private fun setupRecyclerView(){
        giphyAdapter = TrendingGiphyAdapter(object:TrendingGiphyAdapter.OnItemClickListener{
            override fun onFavourite(giphy: Giphy) {
                removeFromFavouriteGiphyLocally(giphy)
            }

        })
        binding.rvFavourite.apply {
            adapter = giphyAdapter
            val numberOfColumns = 2 // Change this to your desired number of columns
            layoutManager = GridLayoutManager(activity,numberOfColumns)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}