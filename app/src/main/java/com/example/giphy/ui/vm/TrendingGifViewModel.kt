package com.example.giphy.ui.vm

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.giphy.dto.response.Giphy
import com.example.giphy.dto.response.GiphyResponse
import com.example.giphy.framework.FLWResponse
import com.example.giphy.framework.Resource
import com.example.giphy.repositories.GiphyRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class TrendingGifViewModel(val repository: GiphyRepository) :
    ViewModel() {

    val trendingGiphy: MutableLiveData<Resource<GiphyResponse>> = MutableLiveData()
    var trendingGiphyPage = 30

    val searchGiphy: MutableLiveData<Resource<GiphyResponse>> = MutableLiveData()
    var searchGiphyPage = 30


    init {
        getTrendyGif()
    }

//    fun getTrendyGif() =
//        viewModelScope.launch(Dispatchers.Main) {
//
//            trendingGiphy.postValue(Resource.Loading())
//
//            val response = repository.getTrendingGiphy(trendingGiphyPage)
//            trendingGiphy.postValue(handleTrendingGiphyResponse(response))
//
//        }

    fun getTrendyGif() {
        viewModelScope.launch(Dispatchers.Main) {
            trendingGiphy.postValue(Resource.Loading())
            repository.getTrendingGiphy(trendingGiphyPage).collect {
                trendingGiphy.postValue(it)
            }
        }
    }

    fun searchGif(searchQuery: String) =
        viewModelScope.launch(Dispatchers.Main) {
            searchGiphy.postValue(Resource.Loading())

            searchGiphy.postValue(Resource.Loading())
            repository.searchGiphy(searchQuery).collect {
                searchGiphy.postValue(it)
            }
        }

    fun saveFavouriteGiphy(giphy: Giphy) = viewModelScope.launch {
        repository.upsert(giphy)
    }

    fun getFavouriteGiphy() = repository.getFavouriteGiphy()

    fun getGiphyById(id: String) = repository.getGiphyById(id)

    fun removeFromFavouriteGiphy(giphy: Giphy) = viewModelScope.launch {
        repository.removeFromFavourite(giphy)
    }

}