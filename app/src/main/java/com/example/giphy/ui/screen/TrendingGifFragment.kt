package com.example.giphy.ui.screen

import android.opengl.Visibility
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.example.giphy.GiphyActivity
import com.example.giphy.adapter.TrendingGiphyAdapter
import com.example.giphy.databinding.FragmentTrendingGifBinding
import com.example.giphy.dto.response.Giphy
import com.example.giphy.framework.Resource
import com.example.giphy.ui.vm.TrendingGifViewModel
import com.example.giphy.util.Constant
import com.example.giphy.util.NetworkUtil
import kotlinx.coroutines.*

class TrendingGifFragment : Fragment() {

    private var _binding: FragmentTrendingGifBinding? = null
    lateinit var viewModel: TrendingGifViewModel
    lateinit var giphyAdapter: TrendingGiphyAdapter

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentTrendingGifBinding.inflate(inflater, container, false)

        viewModel = (activity as GiphyActivity).viewmodel

        setupRecyclerView()
        setupSearchFunction()




        viewModel.trendingGiphy.observe(viewLifecycleOwner, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    hideProgressBar()
                    response.data?.let {
                        giphyAdapter.differ.submitList(it.data)
                    }
                }
                is Resource.Error -> {
                    hideProgressBar()
                    response.message?.let { message ->
                        binding.layoutError.visibility=View.VISIBLE
                        binding.errorMessage.text = message
                        binding.buttonReload.setOnClickListener {
                            MainScope().launch {
                                if(!NetworkUtil.hasInternetConnection(view?.context!!)){
                                    binding.layoutError.visibility=View.VISIBLE
                                }else{
                                    binding.layoutError.visibility=View.INVISIBLE
                                }
                                viewModel.getTrendyGif()
                            }
                        }

                    }
                }
                is Resource.Loading -> {
                    showProgressBar()
                }
                else -> {}
            }
        })

        viewModel.searchGiphy.observe(viewLifecycleOwner, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    hideProgressBar()
                    response.data?.let { giphyResponse ->
                        giphyAdapter.differ.submitList(giphyResponse.data)
                    }
                }
                is Resource.Error -> {
                    hideProgressBar()
                    response.message?.let { message ->
                        Toast.makeText(view?.context, message, Toast.LENGTH_SHORT).show()

                    }
                }
                is Resource.Loading -> {
                    showProgressBar()
                }
                else -> {}
            }
        })

        return binding.root

    }

    private fun saveFavouriteGiphyLocally(it: Giphy) {


        MainScope().launch(Dispatchers.Main) {
            delay(Constant.SEARCH_GIF_TIME_DELAY)

            val giphyItemFromDatabase=viewModel.getGiphyById(it.id)

            if (giphyItemFromDatabase!=null){
                viewModel.removeFromFavouriteGiphy(giphyItemFromDatabase)
                Toast.makeText(view?.context, "Removed From your favourite ", Toast.LENGTH_SHORT).show()
            }else{
                viewModel.saveFavouriteGiphy(it)
                it.isFavourite =true
                Toast.makeText(view?.context, "Added to your favourite ", Toast.LENGTH_SHORT).show()
            }


        }

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    private fun setupSearchFunction() {
        var job: Job? = null
        binding.searchEditText.addTextChangedListener { editabe ->
            job?.cancel()
            job = MainScope().launch {
                delay(Constant.SEARCH_GIF_TIME_DELAY)
                editabe?.let {
                    if (editabe.toString().isNotEmpty()) {
                        viewModel.searchGif(editabe.toString())
                    } else {
                        viewModel.getTrendyGif()
                    }
                }
            }
        }
    }

    private fun hideProgressBar() {
        binding.paginationProgressBar.visibility = View.INVISIBLE
    }

    private fun showProgressBar() {
        binding.paginationProgressBar.visibility = View.VISIBLE
    }

    private fun setupRecyclerView() {
        giphyAdapter = TrendingGiphyAdapter(object: TrendingGiphyAdapter.OnItemClickListener{
            override fun onFavourite(giphy: Giphy) {
                saveFavouriteGiphyLocally(giphy)
            }
        })
        binding.recyclerView.apply {
            adapter = giphyAdapter
            val numberOfColumns = 2 // Change this to your desired number of columns
            layoutManager = GridLayoutManager(activity, numberOfColumns)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}