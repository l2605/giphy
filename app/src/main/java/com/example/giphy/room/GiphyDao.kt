package com.example.giphy.room

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.giphy.dto.response.Giphy

@Dao
interface GiphyDao {

    @Insert(onConflict= OnConflictStrategy.REPLACE)
      fun insertOrUpdate(giphy: Giphy): Long

    @Query("SELECT * FROM giphy")
    fun getAllFavouriteGiphy(): LiveData<List<Giphy>>

    @Query("SELECT * FROM giphy WHERE id = :id LIMIT 1")
     fun getGiphyById(id: String): Giphy?


    @Delete
     fun removeGifFromFav(giphy: Giphy)
}