package com.example.giphy.room

import androidx.room.TypeConverter
import com.example.giphy.dto.response.FixedHeight
import com.example.giphy.dto.response.Images
import com.example.giphy.dto.response.Original
import com.google.gson.Gson

class Converter {
    @TypeConverter
    fun fromImageObject(images: Images): String {
        return Gson().toJson(images)
    }

    @TypeConverter
    fun toImageObject(json: String): Images {
        return Gson().fromJson(json, Images::class.java)
    }

}