package com.example.giphy.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.giphy.dto.response.Giphy

@Database(
    entities = [Giphy::class],
    version = 4
)
@TypeConverters(Converter::class)
abstract class GiphyDatabase : RoomDatabase() {
    abstract fun favouriteGiphyDao(): GiphyDao

    companion object {
        @Volatile
        private var instance: GiphyDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: createDatabase(context).also { instance = it }
        }

        private fun createDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                GiphyDatabase::class.java,
                "fav_giphy.db"
            ).fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build()
    }
}