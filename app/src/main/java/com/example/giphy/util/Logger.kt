package com.example.giphy.util

import timber.log.Timber

class Logger {

    //  val log = AnkoLogger(this.javaClass)


    companion object {

        fun log(message: String) {
            Timber.v(message)
            // logWithASpecificTag.info(message)
        }
    }
}