package com.example.giphy.util

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import org.json.JSONException
import org.json.JSONObject
import timber.log.Timber
import java.lang.reflect.Type
import java.util.HashMap

object GsonUtil {

    fun <T> toGsObject(data: String?, type: Class<T>?): T {
        val gson = Gson()
        return gson.fromJson(data, type)
    }


    fun toGsString(src: Any?): String? {
        if (src == null) {
            return null
        }
        val builder = GsonBuilder()
        builder.setPrettyPrinting()
        val gson = builder.create()
        return gson.toJson(src)
    }



}