package com.example.giphy.repositories

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import com.example.giphy.application.MainApplication
import com.example.giphy.dto.response.Giphy
import com.example.giphy.dto.response.GiphyResponse
import com.example.giphy.framework.FLWResponse
import com.example.giphy.framework.Resource
import com.example.giphy.retrofit.APIService
import com.example.giphy.retrofit.NetworkModule
import com.example.giphy.retrofit.RetroHelper
import com.example.giphy.room.GiphyDatabase
import com.example.giphy.util.Constant
import com.example.giphy.util.Logger
import com.example.giphy.util.NetworkUtil
import kotlinx.coroutines.flow.flow
import retrofit2.Response
import java.net.UnknownHostException

class GiphyRepository {
    private lateinit var context: Context
    private lateinit var application: Application
    private lateinit var apiService: APIService
    private lateinit var database: GiphyDatabase


    companion object {
        @SuppressLint("StaticFieldLeak")
        var instance: GiphyRepository? = null

        fun getInstance(application: Application): GiphyRepository {
            if (instance == null) {
                instance =
                    GiphyRepository(
                        application,
                        NetworkModule.getInstance().provideAPIService(application),
                        GiphyDatabase.invoke(application)
                    )
            }
            return instance!!
        }
    }

    constructor(application: Application, RERPAPIService: APIService,database: GiphyDatabase) {
        this.application = application as MainApplication
        this.apiService = RERPAPIService
        this.context = this.application.applicationContext
        this.database=database
    }

    constructor()

    suspend fun getTrendingGiphy(limit: Int )=  flow<Resource<GiphyResponse>>
    {
        try {
            if (!NetworkUtil.hasInternetConnection(context)) {
                emit(Resource.Error("No Internet Connection"))
                return@flow
            }
            val call = apiService.getTrendyGIF(Constant.API_KEY, limit, 5)
            val helper = RetroHelper<GiphyResponse>()
            helper.enqueue(context, call).collect {
                emit(it)

            }
        }catch (ex: UnknownHostException) {
            Logger.log("${ex.message}")
            emit(Resource.Error(RetroHelper.SERVER_ERROR_MESSAGE))
        } catch (ex: java.lang.Exception) {
            Logger.log("${ex.message}")
            emit(Resource.Error(ex.message))
        }
    }


    suspend fun searchGiphy(searchQuery :String)= flow<Resource<GiphyResponse>>
    {
        try {
            if (!NetworkUtil.hasInternetConnection(context)) {
                emit(Resource.Error("No Internet Connection"))
                return@flow
            }
            val call = apiService.searchTrendyGif(Constant.API_KEY, searchQuery)
            val helper = RetroHelper<GiphyResponse>()
            helper.enqueue(context, call).collect {
                emit(it)

            }
        }catch (ex: UnknownHostException) {
            Logger.log("${ex.message}")
            emit(Resource.Error(RetroHelper.SERVER_ERROR_MESSAGE))
        } catch (ex: java.lang.Exception) {
            Logger.log("${ex.message}")
            emit(Resource.Error(ex.message))
        }
    }


    suspend fun upsert(giphy: Giphy) = database.favouriteGiphyDao().insertOrUpdate(giphy)

    fun getFavouriteGiphy() = database.favouriteGiphyDao().getAllFavouriteGiphy()

    fun getGiphyById(id: String) = database.favouriteGiphyDao().getGiphyById(id)

    suspend fun removeFromFavourite(giphy: Giphy) = database.favouriteGiphyDao().removeGifFromFav(giphy)


}