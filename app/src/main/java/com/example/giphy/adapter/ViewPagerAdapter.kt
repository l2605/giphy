package com.example.giphy.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.giphy.ui.screen.TrendingGifFragment
import com.example.giphy.ui.screen.FavouriteFragment

class ViewPagerAdapter(fragmentActivity: FragmentActivity) : FragmentStateAdapter(fragmentActivity) {
    override fun getItemCount(): Int = 2 // Number of fragments

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> TrendingGifFragment()
            1 -> FavouriteFragment()
            else -> throw IllegalArgumentException("Invalid position: $position")
        }
    }
}
