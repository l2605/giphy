package com.example.giphy.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.example.giphy.R
import com.example.giphy.databinding.TrendyGifItemBinding
import com.example.giphy.dto.response.Giphy
import okhttp3.internal.notify

class TrendingGiphyAdapter(
    val listener: OnItemClickListener
) : RecyclerView.Adapter<TrendingGiphyAdapter.GiphyViewHolder>() {


    interface OnItemClickListener{
        fun onFavourite(giphy:Giphy)
    }


    private val differCallback = object : DiffUtil.ItemCallback<Giphy>() {
        override fun areItemsTheSame(oldItem: Giphy, newItem: Giphy): Boolean {
            return oldItem.url == newItem.url
        }

        override fun areContentsTheSame(oldItem: Giphy, newItem: Giphy): Boolean {
            return oldItem == newItem
        }

    }
    val differ = AsyncListDiffer(this, differCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GiphyViewHolder {
        val binding = TrendyGifItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return GiphyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    override fun onBindViewHolder(holder: GiphyViewHolder, position: Int) {
        val giphyEntity = differ.currentList[position]
        holder.bind(giphyEntity)

    }

    inner class GiphyViewHolder(private val binding: TrendyGifItemBinding) :
        RecyclerView.ViewHolder(binding.root) {


        fun bind(item: Giphy) {
            Glide.with(itemView).load(item.images.fixed_height.url) .transition(
                 DrawableTransitionOptions()).into(binding.itemImage)

                binding.favoriteButton.setOnClickListener{
                listener.onFavourite(item)
                    item.isFavourite = !item.isFavourite
                    if (item.isFavourite){
                        binding.favoriteButton.setBackgroundResource(R.drawable.ic_favourite_checked)
                    }
                    else{
                        binding.favoriteButton.setBackgroundResource(R.drawable.ic_favourite_normal)
                    }
                }

            if (item.isFavourite){
                binding.favoriteButton.setBackgroundResource(R.drawable.ic_favourite_checked)
            }
            else{
                binding.favoriteButton.setBackgroundResource(R.drawable.ic_favourite_normal)
            }


            // Bind other views from the layout using binding
        }
    }
}

