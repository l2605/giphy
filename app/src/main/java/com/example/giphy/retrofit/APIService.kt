package com.example.giphy.retrofit

import com.example.giphy.dto.response.GiphyResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface APIService {

    @GET("gifs/trending")
    suspend fun getTrendyGIF(
        @Query("api_key") api_key: String,
        @Query("limit") limit: Int,
        @Query("offset") offset: Int,
    ): Response<GiphyResponse>

    @GET("gifs/search")
    suspend fun searchTrendyGif(
        @Query("api_key") api_key: String,
        @Query("q") query: String
    ): Response<GiphyResponse>
}