package com.example.giphy.retrofit

import android.content.Context
import com.example.giphy.util.Constant
import com.example.giphy.util.Logger
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit

class NetworkModule {


    companion object {
        var instancee: NetworkModule? = null

        val HTTP_DIR_CACHE = "mapper_cache"
        val CACHE_SIZE = 200 * 1024 * 1024

        fun getInstance(): NetworkModule {
            if (instancee == null) {
                instancee = NetworkModule()
            }
            return instancee!!
        }
    }


    private fun provideCache(context: Context): Cache {
        return Cache(File(context.cacheDir, HTTP_DIR_CACHE), CACHE_SIZE.toLong())
    }

    private fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor =
            HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { message -> Logger.log(message) })
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC
        return httpLoggingInterceptor
    }


    private fun provideOkHttpClient(
        context: Context,
        httpLoggingInterceptor: HttpLoggingInterceptor,
        cache: Cache
    ): OkHttpClient {
        return OkHttpClient.Builder().addInterceptor { chain ->
            val original = chain.request()

//            Timber.v("header " + header!!)
            var request: Request? = null
                request = original.newBuilder()
                    .method(original.method, original.body)
                    .build()

            chain.proceed(request!!)
        }.addInterceptor(httpLoggingInterceptor)
            .readTimeout(2, TimeUnit.MINUTES)
            .connectTimeout(2, TimeUnit.MINUTES)
            .writeTimeout(2, TimeUnit.MINUTES)
            .cache(cache)
            .build()
    }


    private fun provideGson(): Gson {
        return GsonBuilder()
            .serializeNulls()
            .setPrettyPrinting()
            .create()
    }

    private fun provideGsonConverterFactory(gson: Gson): GsonConverterFactory {
        return GsonConverterFactory.create(gson)
    }

    private fun provideRetrofit(
        okHttpClient: OkHttpClient,
        gsonConverterFactory: GsonConverterFactory
    ): Retrofit {
        val builder = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .addConverterFactory(gsonConverterFactory)
        // .addCallAdapterFactory(CoroutineCallAdapterFactory.create());
        return builder.client(okHttpClient).build()
    }

    fun provideAPIService(context: Context): APIService {
        val gsonConverterFactory = provideGsonConverterFactory(provideGson())
        val okHttpClient =
            provideOkHttpClient(context, provideHttpLoggingInterceptor(), provideCache(context))
        val retrofit = provideRetrofit(okHttpClient, gsonConverterFactory)
        return retrofit.create(APIService::class.java)
    }


}