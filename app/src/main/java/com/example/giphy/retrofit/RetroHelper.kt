package com.example.giphy.retrofit

import android.content.Context
import com.example.giphy.dto.response.GiphyResponse
import com.example.giphy.framework.FLWResponse
import com.example.giphy.framework.Resource
import kotlinx.coroutines.flow.flow
import retrofit2.Response

class RetroHelper<T> {


    companion object {
        val SERVER_ERROR_MESSAGE =
            "Server Error. Please check your internet connection and try again."
    }


    suspend fun enqueue(
        context: Context,
        response: Response<T>,
    ) = flow<Resource<T>> {
        try {
            if (response.isSuccessful) {

                emit(Resource.Success(response.body() as T))
            } else {
                emit(Resource.Error(response.message()))
            }
        } catch (ex: Exception) {
            emit(Resource.Error(SERVER_ERROR_MESSAGE))
        }
    }


}